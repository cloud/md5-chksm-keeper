#!/usr/bin/env python3

"""
chksm_keeper.py - Script for computing MD5 checksums of images with non-MD5 url checksum in hiera

Usage:
  git clone https://gitlab.ics.muni.cz/cloud/hiera.git
  python3 ./chksm_keeper.py
"""

import re
import sys
import os
import hashlib
import argparse
import urllib.request

import yaml

# local contants ------------------------------------------------------------
HASH_CALCULATION_BUFFER_SIZE = 1024 * 1024
OUTPUT_MD5SUMS_FILE = 'MD5SUMS'
NON_MD5_LIST = "no_md5_imgs.cfg"
HIERA_IMAGE_FILE = os.path.join(
    os.path.dirname(os.path.realpath(__file__)), "hiera/group/ostack/images.yaml"
)

# local functions -----------------------------------------------------------
def identify_hash(input_string):
    """Function identifies supported hash patterns"""

    # Compiling regex patterns
    sha256_pattern = re.compile("^[A-Fa-f0-9]{64}$")
    sha512_pattern = re.compile("^[A-Fa-f0-9]{128}$")

    # Checking possible patterns
    if sha256_pattern.search(input_string):
        return "sha256"
    elif sha512_pattern.search(input_string):
        return "sha512"
    return None


def obtain_url_chcksm(image_name, url):
    """Function obtains URL checksum files for comparison"""

    # Trying getting url chksm
    try:
        with urllib.request.urlopen(url) as f:
            all_upstream_checksums = f.readlines()

    # Somthing went wrong
    except (urllib.error.HTTPError, urllib.error.URLError) as e:
        print(f"ERROR: Unable to retrieve upstrem checksum file from {url}:")
        print(str(e), file=sys.stderr)
        raise e

    # drop records with leading hashes
    all_upstream_checksums = [i_record.rstrip().decode() for i_record in all_upstream_checksums if not i_record.startswith(b'#')]

    # select matching checksums by name of the image
    matched_upstream_checksum_record = None
    for i_upstream_checksum_record in all_upstream_checksums:
        i_upstream_checksum_record_words = i_upstream_checksum_record.split()
        if image_name in i_upstream_checksum_record_words or f"({image_name})" in i_upstream_checksum_record_words:
            matched_upstream_checksum_record = i_upstream_checksum_record

    for match_splitted in matched_upstream_checksum_record.split():
        hash_match = identify_hash(match_splitted)

        if hash_match != None:
            return match_splitted, hash_match

    print(f"ERROR: Did not find any supported checksum pattern match for {image_name} in online repository")
    exit(1)


def check_image(checksum, hash_type, url):
    """check image content validity, compute MD5 checksum"""
    try:
        img_url_name = os.path.basename(url)
        file_name = img_url_name
        urllib.request.urlretrieve(url, file_name)
    except (urllib.error.HTTPError, urllib.error.URLError) as e:
        print(f"ERROR: Unable to retrieve upstrem checksum file from {url}:")
        print(str(e), file=sys.stderr)
        raise e

    md5_hash_comp = hashlib.md5()
    # select existing hashlib function corresponding to existing upstream hash
    url_hash_comp = getattr(hashlib, hash_type)()

    # Computing hashes through data chunks to prevent loading of whole file
    try:
        with open(file_name, "rb") as f:
            for byte_chunks in iter(lambda: f.read(HASH_CALCULATION_BUFFER_SIZE), b""):
                url_hash_comp.update(byte_chunks)
                md5_hash_comp.update(byte_chunks)
    except OSError as read_exception:
        print(str(read_exception), file=sys.stderr)
        raise read_exception

    md5_checksum = md5_hash_comp.hexdigest()
    upstream_checksum = url_hash_comp.hexdigest()
    if checksum == upstream_checksum:
        print(f"INFO: Image {img_url_name} downloaded OK ({hash_type}:{checksum}, md5:{md5_checksum})")
    else:
        print(f"ERROR: Image {img_url_name} download failed, checksum mismatch (upstream {hash_type}:{checksum} vs. computed {hash_type}:{upstream_checksum})")
        exit(1)

    return md5_checksum


def main():
    """Main steps"""
    try:
        # Importing yaml file with to date images from hiera
        with open(HIERA_IMAGE_FILE, "r", encoding="utf-8") as fh:

            # Reading data
            hiera_imgs = yaml.safe_load(fh)
            hiera_imgs = hiera_imgs["cloud::profile::kolla::glance::os_images"]

            print("INFO: Hiera configuration sucessfully loaded from %s." % HIERA_IMAGE_FILE)
    except yaml.YAMLError as yaml_exception:
        # Something went wrong during reading
        print(str(yaml_exception), file=sys.stderr)
        raise yaml_exception

    non_md5_cfg = {}

    try:
        # Importing yaml file with to date images from hiera
        with open(NON_MD5_LIST, "r", encoding="utf-8") as fh:

            for line in fh:
                #skipping possible empty lines
                if(line.strip() == ''):
                    continue

                # Reading config file(names and url)
                img_name_cfg, repo_cfg = line.strip().replace("\n", "").split()

                # Reading data
                non_md5_cfg[img_name_cfg] = repo_cfg

            print("INFO: Non MD5 image list successfully loaded from %s." % NON_MD5_LIST)
    except OSError as txt_exception:
        print(str(txt_exception), file=sys.stderr)
        raise txt_exception

    url_imgs_data = []
    for image in hiera_imgs:
        if image["name"] in non_md5_cfg:

            # Gatheering URL checksum data - checksum URL, basename URL, source image URL
            url_imgs_data.append(
                ",".join(
                    [
                        non_md5_cfg[image["name"]],
                        os.path.basename(image["url"]),
                        image["url"],
                    ]
                )
            )

    # Eliminating possible duplicates
    url_imgs_data = list(set(url_imgs_data))
    url_imgs_data = [item.split(",") for item in url_imgs_data]

    print("INFO: Plan to recompute content of following images %s." % [i[1] for i in url_imgs_data])

    # MD5 hashes
    md5_hashes = []
    for chksm_url, url_basename, source_url in url_imgs_data:

        chksm, hash_type = obtain_url_chcksm(url_basename, chksm_url)

        # Checking image chksms, hash_type, url for download
        md5_hash = check_image(chksm, hash_type, source_url)
        md5_hashes.append(f"{md5_hash}  {url_basename}")

    try:
        with open(OUTPUT_MD5SUMS_FILE, "w") as fh:
            fh.write("\n".join(md5_hashes))
        print("INFO: Checksum file generated as %s." % OUTPUT_MD5SUMS_FILE)
    except OSError as md5_write_exception:
        print(str(md5_write_exception), file=sys.stderr)
        raise md5_write_exception

if __name__ == "__main__":
    main()
